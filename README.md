## Deployment and development

#### Technology stack
- Java 11
- Spring Boot 2.5
- Gradle 6.2
- JUnit 5
- JaCoCo
- JavaFX
- Lombok

#### Setup in IDE
- Import as Gradle project
- Mark src/main/java as sources root
- Mark src/test/java as test sources root
- Enable annotation processing in IDE (if applicable)
- Build using Gradle
- Run CleanTextApplication.java, in src/main/java/ee/bernard/cleantext

#### Setup from command line

**Running unit tests and generating test report**
	
	gradlew test

Test report is copied to:

    ./release/test_report

**Building and creating executable**
	
	gradle build

JAR file is created in:

    ./release/

#### Running from jar

A runnable JAR file is generated in the ./release folder, when the
application is built and is included in this repository.

To execute the JAR, Java JVM version 11 or later is required on the
host machine.

*NB! JAR and UI elements have so far been tested in Windows only!*

#### Basic usage

The application uses one view. Text can be inserted by typing it or
using system clipboard. Right-clicking the text field will bring up
a context menu with four options: 'Copy', 'Cut', 'Paste' and
'Select All', each has their respective keyboard shortcuts as well.

In addition all system shortcuts for manipulating the clipboard should
work as well (tested in Windows only!).

------
------
# Design

Since Apimac Clean Text is a Mac-only software (no Windows version was
available) original functionality could not be tested or verified.

Where possible documentation from Apimac was used and the following
2015 video of an old version was referenced:
https://www.youtube.com/watch?v=bRLTVbID4GU
 
However no proper user manual or functional specifications could be found.

Overall, best guesses where made as to what each function actually does.
Design decisions are documented below.

## Undo:

Although not in original scope, to expedite testing a simple undo functionality
was implemented. The stack retains and undoes only function calls, not manual
edits inside the text field.

## Fix:

The most difficult category of functions to interpret without being
able to reference the original software or any documentation.  

- #### Fix Spaces
> Leading and trailing spaces of each line  will be trimmed, multiple
> consecutive spaces inside each line will be reduced to single space.

- #### Fix Line Breaks
> The "fix paragraphs" functionality as seen in the 2015 video is
> implemented. All line endings that are not proceeded by an empty line
> (i.e. end of paragraph) are removed, hence consolidating paragraphs.
> 
> All consecutive empty lines are removed between paragraphs and
> replaced with a single empty line.

- #### Join Paragraphs 
> Unclear what exactly is meant here.
>
> For repairing disjointed paragraphs (paragraphs with dispersed line breaks)
> the "Fix line breaks" function could be used.
>
> For joining all lines the "remove returns" function could be used. 
>
> So currently this function  removes empty lines between paragraphs,
> essentially "joining" them. Line breaks at the end of each line
> are retained.


## Remove:

- #### Spaces
> Removes all spaces from text.

- #### Returns
> Removes all line breaks from text.

- #### Spaces and Returns
> Removes both spaces and line breaks from text.

- #### Quote Prefix
> '>' character is assumed to represent quote prefix. Function removes
> only leading quote prefixes and spaces from each line. '>' characters
> inside a line are not removed.

- #### Empty Lines
> All consecutive line breaks are replaced with a single line break

- #### Line Numbers
> It is assumed that line numbers are at the beginning of each line,
> may or may not have a colon after them and are separated by spaces
> from the line itself. Removes those numbers and spaces from each line.

- #### Duplicate Lines
> Only consecutive duplicate lines are removed. If a duplicate is
> present anywhere else in the text, it will not be deleted.


## Trim:

As per:
http://apimac.freshdesk.com/support/solutions/articles/4000116648-what-is-the-difference-between-remove-spaces-and-trim-spaces-

Trim group functions only trim the beginning and end of the string,
not each line. So if there are line breaks inside the string,
characters before and after it are considered part of the text and
not trimmed. 

- #### Spaces
> Removes spaces but not empty lines from beginning and end of string.

- #### Empty Lines
> Removes empty lines but not spaces from beginning and end of string.

- #### Spaces and Empty Lines
> Removes both spaces and empty lines from beginning and end of string.


## Replace:

Self explanatory functions. Replace one string with another across all input.

- #### Tabs With Spaces
- #### Spaces With Tabs
- #### Tabs With Four Spaces
- #### Four Spaces With Tab
- #### Ellipsis to Three Periods
- #### Three Periods to Ellipsis
