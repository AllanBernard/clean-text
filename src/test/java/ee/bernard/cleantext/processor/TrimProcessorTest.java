package ee.bernard.cleantext.processor;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class TrimProcessorTest {

    private static final String OUTPUT_NOT_EMPTY_MESSAGE = "Output text should not be empty";

    @Test
    public void testTrimProcessorShouldNotBeInstantiated() throws NoSuchMethodException {
        Constructor<TrimProcessor> constructor = TrimProcessor.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        assertAll("Instance should not be created",
                () -> assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Constructor should have private access modifier"),
                () -> assertThrows(InvocationTargetException.class, () -> constructor.newInstance(), "Should throw correct Exception")
        );
    }

    @Test
    public void testTrimSpacesFromString() {
        String input = new StringBuilder(" ").append(System.lineSeparator())
                .append(" test ").append(System.lineSeparator())
                .append(" ")
                .toString();
        String expected = new StringBuilder(System.lineSeparator())
                .append(" test ").append(System.lineSeparator())
                .toString();
        String exception = "test";


        String output = TrimProcessor.trimSpaces(input);


        assertAll("Leading and trailing spaces in text should be correctly processed:",
                () -> assertNotNull(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Leading and trailing spaces should be trimmed"),
                () -> assertNotEquals(expected, exception, "Empty lines should not be trimmed")
        );
    }

    @Test
    public void testTrimTabsFromString() {
        String input = "\t\t test \t\t";
        String expected = " test ";


        String output = TrimProcessor.trimTabs(input);


        assertAll("Leading and trailing spaces in text should be correctly processed:",
                () -> assertNotNull(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Leading and trailing tabs should be trimmed")
        );
    }

    @Test
    public void testTrimEmptyLinesFromString() {
        String singleLineInput = new StringBuilder(System.lineSeparator())
                .append(" test ").append(System.lineSeparator())
                .toString();
        String multiLineInput = new StringBuilder(System.lineSeparator())
                .append(System.lineSeparator())
                .append("Line 1").append(System.lineSeparator())
                .append("Line 2").append(System.lineSeparator())
                .append(System.lineSeparator())
                .toString();
        String singleLineExpected = " test ";
        String multiLineExpected = new StringBuilder("Line 1").append(System.lineSeparator())
                .append("Line 2")
                .toString();
        String exception = "test";


        String singleLineOutput = TrimProcessor.trimEmptyLines(singleLineInput);
        String multiLineOutput = TrimProcessor.trimEmptyLines(multiLineInput);


        assertAll("Empty lines in text should be correctly processed:",
                () -> assertFalse(singleLineOutput.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(singleLineExpected, singleLineOutput, "Empty lines should be trimmed"),
                () -> assertNotEquals(singleLineExpected, exception, "Spaces should not be trimmed"),
                () -> assertEquals(multiLineExpected, multiLineOutput, "Empty lines should be trimmed")
        );
    }

    @Test
    public void testUseCaseTrimSpacesAndEmptyLines() {
        String input = new StringBuilder(System.lineSeparator())
                .append(System.lineSeparator())
                .append("  test  ").append(System.lineSeparator())
                .append(System.lineSeparator())
                .toString();
        String expected = "test";


        String output = TrimProcessor.trimAndClear(input);


        assertAll("All spaces and empty lines should be trimmed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Formatting should be correct")
        );
    }

}
