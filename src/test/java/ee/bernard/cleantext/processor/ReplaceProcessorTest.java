package ee.bernard.cleantext.processor;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class ReplaceProcessorTest {

    private static final String OUTPUT_NOT_EMPTY_MESSAGE = "Output text should not be empty";

    @Test
    public void testReplaceProcessorShouldNotBeInstantiated() throws NoSuchMethodException {
        Constructor<ReplaceProcessor> constructor = ReplaceProcessor.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        assertAll("Instance should not be created",
                () -> assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Constructor should have private access modifier"),
                () -> assertThrows(InvocationTargetException.class, () -> constructor.newInstance(), "Should throw correct Exception")
        );
    }

    @Test
    public void testShouldReplaceTabWithSpace() {
        String input = "\t\t";
        String expected = "  ";


        String output = ReplaceProcessor.replaceTabWithSpace(input);


        assertAll("Tabs in text should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Tab should be replaced with space")
        );
    }

    @Test
    public void testShouldReplaceSpaceWithTab() {
        String input = "  ";
        String expected = "\t\t";


        String output = ReplaceProcessor.replaceSpaceWithTab(input);


        assertAll("Spaces in text should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Space should be replaced with tab")
        );
    }

    @Test
    public void testShouldReplaceTabsWithFourSpaces() {
        String input = "\t\t";
        String expected = "        ";


        String output = ReplaceProcessor.replaceTabsWithFourSpaces(input);


        assertAll("Tabs in text should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Tab should be replaced with four spaces")
        );
    }

    @Test
    public void testShouldReplaceFourSpacesWithTabs() {
        String input = "        ";
        String expected = "\t\t";


        String output = ReplaceProcessor.replaceFourSpacesWithTabs(input);


        assertAll("Four spaces in text should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Four spaces should be replaced with tabs")
        );
    }

    @Test
    public void testShouldReplaceEllipsisWithThreePeriods() {
        String input = "\u2026\u2026";
        String expected = "......";


        String output = ReplaceProcessor.replaceEllipsisWithThreePeriods(input);


        assertAll("Ellipsis (u2026) should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Ellipsis should be replaced with three periods")
        );
    }

    @Test
    public void testShouldReplaceThreePeriodsWithEllipsis() {
        String input = "......";
        String expected = "\u2026\u2026";


        String output = ReplaceProcessor.replaceThreePeriodsWithEllipsis(input);


        assertAll("Three periods should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Three periods should be replaced with an Ellipsis (u2026)")
        );
    }

}
