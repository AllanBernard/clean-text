package ee.bernard.cleantext.processor;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class FixProcessorTest {

    private static final String OUTPUT_NOT_EMPTY_MESSAGE = "Output text should not be empty";
    private static final String FORMAT_NOT_CORRECT_MESSAGE = "Format should be correct";

    @Test
    public void testFixProcessorShouldNotBeInstantiated() throws NoSuchMethodException {
        Constructor<FixProcessor> constructor = FixProcessor.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        assertAll("Instance should not be created",
                () -> assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Constructor should have private access modifier"),
                () -> assertThrows(InvocationTargetException.class, () -> constructor.newInstance(), "Should throw correct Exception")
        );
    }

    @Test
    public void testShouldFixSpaces() {
        String input = new StringBuilder(" There are  too   many    spaces.   ").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("  Too  many.   ")
                .toString();
        String expected = new StringBuilder("There are too many spaces.").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("Too many.")
                .toString();


        String output = FixProcessor.fixSpaces(input);


        assertAll("Duplicate spaces should be removed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldFixLineBreaks() {
        String input = "\n\na\r\nb\nc\r\rd\r  \t \t  \re\n\r\r\r\rf\r\r";
        String expected = new StringBuilder("abc").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("d").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("e").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("f")
                .toString();
        input = ProcessorUtil.convertLineEndingsToSystem(input);


        String output = FixProcessor.fixLineBreaks(input);


        assertAll("Line breaks should be fixed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldConcatLinesToParagraphs() {
        String input = new StringBuilder("Beginning of the first paragraph, ").append(System.lineSeparator())
                .append("on wrong line ").append(System.lineSeparator())
                .append("and another one on wrong line. End of paragraph.").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("Beginning of the second paragraph, ").append(System.lineSeparator())
                .append("on wrong line. End of second paragraph.").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("Third paragraph after double return.").append(System.lineSeparator())
                .append(System.lineSeparator())
                .toString();
        String expectedFirstParagraph = "Beginning of the first paragraph, on wrong line and another one on wrong line. End of paragraph.\r\n";
        String expectedSecondParagraph = "Beginning of the second paragraph, on wrong line. End of second paragraph.\r\n";
        String expectedThirdParagraph = "Third paragraph after double return.\r\n";
        String expected = new StringBuilder()
                .append(expectedFirstParagraph).append(System.lineSeparator())
                .append(expectedSecondParagraph).append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(expectedThirdParagraph).append(System.lineSeparator())
                .toString();


        String output = FixProcessor.concatLinesToParagraphs(input);


        assertAll("Paragraphs should be fixed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldJoinParagraphs() {
        String input = new StringBuilder("a").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("b").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("c").append(System.lineSeparator())
                .toString();
        String expected = new StringBuilder("a").append(System.lineSeparator())
                .append("b").append(System.lineSeparator())
                .append("c").append(System.lineSeparator())
                .toString();


        String output = FixProcessor.joinParagraphs(input);


        assertAll("Paragraphs should be joined:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

}
