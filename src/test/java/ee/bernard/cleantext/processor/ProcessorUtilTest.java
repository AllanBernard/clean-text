package ee.bernard.cleantext.processor;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class ProcessorUtilTest {

    private static final String OUTPUT_NOT_EMPTY_MESSAGE = "Output text should not be empty";
    private static final String FORMAT_NOT_CORRECT_MESSAGE = "Format should be correct";

    @Test
    public void testProcessorUtilShouldNotBeInstantiated() throws NoSuchMethodException {
        Constructor<ProcessorUtil> constructor = ProcessorUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        assertAll("Instance should not be created",
                () -> assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Constructor should have private access modifier"),
                () -> assertThrows(InvocationTargetException.class, () -> constructor.newInstance(), "Should throw correct Exception")
        );
    }

    @Test
    public void testLineEndingsShouldBeUnifiedToSystem() {
        String input = new StringBuilder()
                .append("This is a Windows line\r\n")
                .append("This is a Unix line\r")
                .append("This is a Mac line\n")
                .toString();
        String expected = new StringBuilder()
                .append("This is a Windows line").append(System.lineSeparator())
                .append("This is a Unix line").append(System.lineSeparator())
                .append("This is a Mac line").append(System.lineSeparator())
                .toString();


        String output = ProcessorUtil.convertLineEndingsToSystem(input);


        assertAll(
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testLineEndingsShouldBeConvertedToWinFormat() {
        String input = new StringBuilder()
                .append("This is a Windows line\r\n")
                .append("This is a Unix line\r")
                .append("This is a Mac line\n")
                .toString();
        String expected = new StringBuilder()
                .append("This is a Windows line\r\n")
                .append("This is a Unix line\r\n")
                .append("This is a Mac line\r\n")
                .toString();


        String output = ProcessorUtil.convertLineEndingsToWin(input);


        assertAll(
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testStringShouldBeSplitToAnArrayOfLines() {
        String emptyInput = "";
        String singleLineInput = "test";
        String endingWithLineBreak = new StringBuilder("a").append(System.lineSeparator())
                .append("b").append(System.lineSeparator())
                .toString();
        String endingWithoutLineBreak = new StringBuilder("a").append(System.lineSeparator())
                .append("b")
                .toString();
        String[] emptyExpected = {""};
        String[] singleLineExpected = {"test"};
        String[] endingWithLineBreakExpected = {"a", "b", ""};
        String[] endingWithoutLineBreakExpected = {"a", "b"};


        String[] emptyOutput = ProcessorUtil.createArrayOfLines(emptyInput);
        String[] singleLineOutput = ProcessorUtil.createArrayOfLines(singleLineInput);
        String[] endingWithLineBreakOutput = ProcessorUtil.createArrayOfLines(endingWithLineBreak);
        String[] endingWithoutLineBreakOutput = ProcessorUtil.createArrayOfLines(endingWithoutLineBreak);


        assertAll("Input string should be mapped to array, using returns; ",
                () -> assertArrayEquals(emptyExpected, emptyOutput, "Array should have one empty line"),
                () -> assertArrayEquals(singleLineExpected, singleLineOutput, "Array should have one line"),
                () -> assertArrayEquals(endingWithLineBreakExpected, endingWithLineBreakOutput, "Array should terminate with an empty line"),
                () -> assertArrayEquals(endingWithoutLineBreakExpected, endingWithoutLineBreakOutput, "Array should not terminate with an empty line")
        );
    }

    @Test
    public void testArrayShouldBeJoinedToMultilineString() {
        String[] endWithLineBreak = {"", "a", "b", "", "c", ""};
        String[] endWithoutLineBreak = {"a", "b"};
        String[] isEmpty = {""};
        String expectedEndWithLineBreak = new StringBuilder(System.lineSeparator())
                .append("a").append(System.lineSeparator())
                .append("b").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("c").append(System.lineSeparator())
                .toString();
        String expectedEndWithoutLineBreak = new StringBuilder("a")
                .append(System.lineSeparator())
                .append("b")
                .toString();


        String outputEndWithLineBreak = ProcessorUtil.createStringOfLines(endWithLineBreak);
        String outputEndWithoutLineBreak = ProcessorUtil.createStringOfLines(endWithoutLineBreak);
        String emptyOutput = ProcessorUtil.createStringOfLines(isEmpty);


        assertAll("String should be created from lines: ",
                () -> assertEquals(expectedEndWithLineBreak, outputEndWithLineBreak, "Line should end with a return"),
                () -> assertEquals(expectedEndWithoutLineBreak, outputEndWithoutLineBreak, "Line should end without a return"),
                () -> assertEquals("", emptyOutput, "Empty input should produce empty output")
        );
    }

    @Test
    public void testShouldReplaceConsecutiveCharacters() {


        String doubleOutput = ProcessorUtil.removeConsecutiveCharacters("Foo Bar", "o", 2);
        String tripleOutput = ProcessorUtil.removeConsecutiveCharacters("Fooo Bar", "o", 3);


        assertAll("Consecutive characters should be replaced with one character less:",
                () -> assertFalse(doubleOutput.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals("Fo Bar", doubleOutput, FORMAT_NOT_CORRECT_MESSAGE),
                () -> assertEquals("Foo Bar", tripleOutput, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldClearEmptyLines() {
        String input = new StringBuilder("abc").append(System.lineSeparator())
                .append("  \t \t  ").append(System.lineSeparator())
                .append("def")
                .toString();
        String expected = new StringBuilder("abc").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("def")
                .toString();


        String output = ProcessorUtil.clearEmptyLines(input);


        assertEquals(expected, output);
    }

    @Test
    public void testShouldIdentifyLastFilledLineInArray() {
        String[] inputArray = {"a", "b", "", "", "c", "", ""};


        boolean shouldReturnFalse = ProcessorUtil.isLastFilledLineInArray(inputArray, 2);
        boolean shouldReturnTrue = ProcessorUtil.isLastFilledLineInArray(inputArray, 4);


        assertAll("Last filled line in array should be identified:",
                () -> assertFalse(shouldReturnFalse, "line incorrectly identified as last"),
                () -> assertTrue(shouldReturnTrue, "last filled line not correctly identified")
        );
    }

    @Test
    public void testShouldIdentifyEndOfParagraphs() {
        String[] inputArray = {"a", "b", "", "", "c", "", ""};


        boolean shouldReturnFalse = ProcessorUtil.isEndOfParagraph(inputArray, 0);
        boolean shouldReturnTrue = ProcessorUtil.isEndOfParagraph(inputArray, 1);


        assertAll("End of paragraph should be identified:",
                () -> assertFalse(shouldReturnFalse, "line incorrectly identified as end of paragraph"),
                () -> assertTrue(shouldReturnTrue, "end of paragraph not correctly identified")
        );
    }

    @Test
    public void testShouldIdentifySecondEmptyLine() {
        String[] tooShort = {""};
        String[] twoEmptyLines = {"", ""};
        String[] inputArray = {"a", "b", "", "", "", "c", "", ""};


        assertAll("Second (double) empty line should be identified:",
                () -> assertFalse(ProcessorUtil.isSecondEmptyLineInArray(tooShort, 0), "Single empty line incorrectly identified"),
                () -> assertTrue(ProcessorUtil.isSecondEmptyLineInArray(twoEmptyLines, 1), "Two empty lines not correctly identified"),
                () -> assertFalse(ProcessorUtil.isSecondEmptyLineInArray(inputArray, 5), "Line incorrectly identified as double empty"),
                () -> assertFalse(ProcessorUtil.isSecondEmptyLineInArray(inputArray, 7), "Line incorrectly identified as double empty"),
                () -> assertTrue(ProcessorUtil.isSecondEmptyLineInArray(inputArray, 4), "Double empty line not correctly identified")
        );
    }

    @Test
    public void testShouldIdentifyLineWithoutVisibleCharacters() {
        String inputWithoutVisibleCharacters = "  \t   \t  ";
        String inputWithVisibleCharacters = "   \t  test  \t  ";


        boolean emptyLineShouldReturnTrue = ProcessorUtil.hasNoVisibleCharacters("");
        boolean shouldReturnTrue = ProcessorUtil.hasNoVisibleCharacters(inputWithoutVisibleCharacters);
        boolean shouldReturnFalse = ProcessorUtil.hasNoVisibleCharacters(inputWithVisibleCharacters);


        assertAll("Lines without visible character shoulb beidentified:",
                () -> assertTrue(emptyLineShouldReturnTrue, "Empty line not correctly identified"),
                () -> assertTrue(shouldReturnTrue, "Line without visible characters not correctly identified"),
                () -> assertFalse(shouldReturnFalse, "Line with visible characters incorrectly identified")
        );
    }

}
