package ee.bernard.cleantext.processor;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class RemoveProcessorTest {

    private static final String OUTPUT_NOT_EMPTY_MESSAGE = "Output text should not be empty";
    private static final String FORMAT_NOT_CORRECT_MESSAGE = "Format should be correct";

    @Test
    public void testRemoveProcessorShouldNotBeInstantiated() throws NoSuchMethodException {
        Constructor<RemoveProcessor> constructor = RemoveProcessor.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        assertAll("Instance should not be created",
                () -> assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Constructor should have private access modifier"),
                () -> assertThrows(InvocationTargetException.class, () -> constructor.newInstance(), "Should throw correct Exception")
        );
    }

    @Test
    public void testShouldRemoveSpaces() {
        String input = " this is a test string with spaces ";
        String expected = "thisisateststringwithspaces";


        String output = RemoveProcessor.removeSpaces(input);


        assertAll("All spaces in text should be removed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldRemoveReturns() {
        String input = new StringBuilder("begin ")
                .append(System.lineSeparator())
                .append("\r")
                .append("\n")
                .append("\r\n")
                .append(" end")
                .toString();
        String expected = "begin  end";


        String output = RemoveProcessor.removeReturns(input);


        assertAll("All return (new line) characters should be removed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldRemoveSpacesAndReturns() {
        String input = new StringBuilder("abc ")
                .append(System.lineSeparator())
                .append("\r")
                .append("\n")
                .append("\r\n")
                .append(" def")
                .toString();
        String expected = "abcdef";


        String output = RemoveProcessor.removeSpacesAndReturns(input);


        assertAll("All return (new line) characters and spaces should be removed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldRemoveQuotePrefix() {
        String input = new StringBuilder(">>>   What if a > b?  ").append(System.lineSeparator())
                .append(">> and b > a?").append(System.lineSeparator())
                .append(">>or c > d").append(System.lineSeparator())
                .toString();
        String expected = new StringBuilder("What if a > b?  ").append(System.lineSeparator())
                .append("and b > a?").append(System.lineSeparator())
                .append("or c > d").append(System.lineSeparator())
                .toString();


        String output = RemoveProcessor.removeQuotePrefix(input);


        assertAll("Quote prefixes should be removed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testRemoveEmptyLinesFromString() {
        String input = new StringBuilder("test ").append(System.lineSeparator())
                .append(" test2 ").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(" test3 ").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(" test4").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .toString();
        String expected = new StringBuilder("test ").append(System.lineSeparator())
                .append(" test2 ").append(System.lineSeparator())
                .append(" test3 ").append(System.lineSeparator())
                .append(" test4")
                .toString();


        String output = RemoveProcessor.removeEmptyLines(input);


        assertAll("Empty lines in text should be correctly processed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, "Empty lines should be removed from string")
        );
    }

    @Test
    public void testShouldRemoveLineNumbers() {
        String input = new StringBuilder("1  a").append(System.lineSeparator())
                .append("2  b").append(System.lineSeparator())
                .append("3  ").append(System.lineSeparator())
                .append("4: test").append(System.lineSeparator())
                .append("50 5")
                .toString();
        String expected = new StringBuilder("a").append(System.lineSeparator())
                .append("b").append(System.lineSeparator())
                .append(System.lineSeparator())
                .append("test").append(System.lineSeparator())
                .append("5")
                .toString();


        String output = RemoveProcessor.removeLineNumbers(input);


        assertAll("Line numbers should be removed:",
                () -> assertFalse(output.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(expected, output, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

    @Test
    public void testShouldRemoveDuplicateLines() {
        String singleLineInput = "This is a line";
        String multiLineInput = new StringBuilder()
                .append("This is a line").append(System.lineSeparator())
                .append("This is a duplicate line").append(System.lineSeparator())
                .append("This is a duplicate line").append(System.lineSeparator())
                .append("This is a line").append(System.lineSeparator())
                .append("This is a triplicate line").append(System.lineSeparator())
                .append("This is a triplicate line").append(System.lineSeparator())
                .append("This is a triplicate line").append(System.lineSeparator())
                .toString();
        String multiLineExpected = new StringBuilder()
                .append("This is a line").append(System.lineSeparator())
                .append("This is a duplicate line").append(System.lineSeparator())
                .append("This is a line").append(System.lineSeparator())
                .append("This is a triplicate line").append(System.lineSeparator())
                .toString();


        String singleLineOutput = RemoveProcessor.removeDuplicateLines(singleLineInput);
        String multiLineOutput = RemoveProcessor.removeDuplicateLines(multiLineInput);


        assertAll("Duplicates lines should be removed:",
                () -> assertFalse(multiLineOutput.isEmpty(), OUTPUT_NOT_EMPTY_MESSAGE),
                () -> assertEquals(multiLineExpected, multiLineOutput, FORMAT_NOT_CORRECT_MESSAGE),
                () -> assertEquals(singleLineInput, singleLineOutput, FORMAT_NOT_CORRECT_MESSAGE)
        );
    }

}
