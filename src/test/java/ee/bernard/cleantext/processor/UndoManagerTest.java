package ee.bernard.cleantext.processor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UndoManagerTest {

    @Test
    public void testTextShouldBeAddedToUndoStack() {
        UndoManager undoManager = new UndoManager();


        String noOutput = undoManager.undoEdit();
        boolean beforeAdd = undoManager.canUndo();
        undoManager.addState("test");
        boolean afterAdd = undoManager.canUndo();
        String undoOutput = undoManager.undoEdit();
        boolean afterUndo = undoManager.canUndo();


        assertAll("Undo manager stack should be working:",
                () -> assertNull(noOutput, "Empty undo stack should return null state"),
                () -> assertFalse(beforeAdd, "Undo stack should start empty"),
                () -> assertTrue(afterAdd, "Undo stack should retain edits"),
                () -> assertEquals("test", undoOutput, "Undo stack should return last state"),
                () -> assertFalse(afterUndo, "Undo stack should finish empty")
        );
    }

}
