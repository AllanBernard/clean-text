package ee.bernard.cleantext.configuration;

import ee.bernard.cleantext.gui.MainFrame;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:application.properties")
public class SpringContext {

    @Value("${MainWindow.Title:Clean Text}")
    String title;
    @Value("${MainWindow.Height:800}")
    int mainWindowHeight;
    @Value("${MainWindow.Width:1200}")
    int mainWindowWidth;
    @Value("${ButtonComponent.Width:210}")
    int buttonComponentWidth;
    @Value("${TextComponent.WordWrap:true}")
    boolean textComponentWordWrap;
    @Value("${TextComponent.FontSize:15}")
    float textComponentFontSize;

    @Bean
    public static PropertySourcesPlaceholderConfigurer setUp() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(name = "mainFrame")
    public MainFrame createMainFrame() {
        MainFrame mf = new MainFrame();
        mf.setTitle(title);
        mf.setMainWindowHeight(mainWindowHeight);
        mf.setMainWindowWidth(mainWindowWidth);
        mf.setButtonComponentWidth(buttonComponentWidth);
        mf.setTextComponentShouldWrap(textComponentWordWrap);
        mf.setTextComponentFontSize(textComponentFontSize);
        return mf;
    }
}
