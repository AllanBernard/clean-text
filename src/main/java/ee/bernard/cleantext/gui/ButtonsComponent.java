package ee.bernard.cleantext.gui;

import lombok.Getter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

import static java.awt.Component.RIGHT_ALIGNMENT;

@Getter
public class ButtonsComponent {

    private JPanel component;

    public ButtonsComponent(int componentHeight, int componentWidth, ActionListener actionListener) {
        component = new JPanel();
        component.setPreferredSize(new Dimension(componentWidth, componentHeight));
        component.setAlignmentX(RIGHT_ALIGNMENT);
        GridLayout gridLayout = new GridLayout(24, 1);
        gridLayout.setVgap(5);
        component.setLayout(gridLayout);

        JButton undo = new JButton(CommandEnum.UNDO.toString());
        JButton fixSpaces = new JButton(CommandEnum.FIX_SPACES.toString());
        JButton fixLineBreaks = new JButton(CommandEnum.FIX_LINE_BREAKS.toString());
        JButton joinParagraphs = new JButton(CommandEnum.JOIN_PARAGRAPHS.toString());
        JButton removeSpaces = new JButton(CommandEnum.REMOVE_SPACES.toString());
        JButton removeReturns = new JButton(CommandEnum.REMOVE_RETURNS.toString());
        JButton removeSpacesAndReturns = new JButton(CommandEnum.REMOVE_SPACES_AND_RETURNS.toString());
        JButton removeQuotePrefix = new JButton(CommandEnum.REMOVE_QUOTE_PREFIX.toString());
        JButton removeEmptyLines = new JButton(CommandEnum.REMOVE_EMPTY_LINES.toString());
        JButton removeLineNumbers = new JButton(CommandEnum.REMOVE_LINE_NUMBERS.toString());
        JButton removeDuplicateLines = new JButton(CommandEnum.REMOVE_DUPLICATE_LINES.toString());
        JButton trimSpaces = new JButton(CommandEnum.TRIM_SPACES.toString());
        JButton trimEmptyLines = new JButton(CommandEnum.TRIM_EMPTY_LINES.toString());
        JButton trimSpacesAndEmptyLines = new JButton(CommandEnum.TRIM_SPACES_AND_EMPTY_LINES.toString());
        JButton replaceTabsWithSpaces = new JButton(CommandEnum.REPLACE_TABS_WITH_SPACES.toString());
        JButton replaceSpacesWithTabs = new JButton(CommandEnum.REPLACE_SPACES_WITH_TABS.toString());
        JButton replaceTabsWithFourSpaces = new JButton(CommandEnum.REPLACE_TABS_WITH_FOUR_SPACES.toString());
        JButton replaceFourSpacesWithTab = new JButton(CommandEnum.REPLACE_FOUR_SPACES_WITH_TAB.toString());
        JButton replaceEllipsisToThreePeriods = new JButton(CommandEnum.REPLACE_ELLIPSIS_TO_THREE_PERIODS.toString());
        JButton replaceThreePeriodsToEllipsis = new JButton(CommandEnum.REPLACE_THREE_PERIODS_TO_ELLIPSIS.toString());

        undo.addActionListener(actionListener);
        undo.setEnabled(false);
        fixSpaces.addActionListener(actionListener);
        fixLineBreaks.addActionListener(actionListener);
        joinParagraphs.addActionListener(actionListener);
        removeSpaces.addActionListener(actionListener);
        removeReturns.addActionListener(actionListener);
        removeSpacesAndReturns.addActionListener(actionListener);
        removeQuotePrefix.addActionListener(actionListener);
        removeEmptyLines.addActionListener(actionListener);
        removeLineNumbers.addActionListener(actionListener);
        removeDuplicateLines.addActionListener(actionListener);
        trimSpaces.addActionListener(actionListener);
        trimEmptyLines.addActionListener(actionListener);
        trimSpacesAndEmptyLines.addActionListener(actionListener);
        replaceTabsWithSpaces.addActionListener(actionListener);
        replaceSpacesWithTabs.addActionListener(actionListener);
        replaceTabsWithFourSpaces.addActionListener(actionListener);
        replaceFourSpacesWithTab.addActionListener(actionListener);
        replaceEllipsisToThreePeriods.addActionListener(actionListener);
        replaceThreePeriodsToEllipsis.addActionListener(actionListener);

        component.add(undo);
        component.add(new JLabel("FIX"));
        component.add(fixSpaces);
        component.add(fixLineBreaks);
        component.add(joinParagraphs);
        component.add(new JLabel("REMOVE"));
        component.add(removeSpaces);
        component.add(removeReturns);
        component.add(removeSpacesAndReturns);
        component.add(removeQuotePrefix);
        component.add(removeEmptyLines);
        component.add(removeLineNumbers);
        component.add(removeDuplicateLines);
        component.add(new JLabel("TRIM"));
        component.add(trimSpaces);
        component.add(trimEmptyLines);
        component.add(trimSpacesAndEmptyLines);
        component.add(new JLabel("REPLACE"));
        component.add(replaceTabsWithSpaces);
        component.add(replaceSpacesWithTabs);
        component.add(replaceTabsWithFourSpaces);
        component.add(replaceFourSpacesWithTab);
        component.add(replaceEllipsisToThreePeriods);
        component.add(replaceThreePeriodsToEllipsis);
    }

    public void UndoEnabled(boolean undoEnabled) {
        component.getComponent(0).setEnabled(undoEnabled);
    }

}
