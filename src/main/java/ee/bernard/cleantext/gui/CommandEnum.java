package ee.bernard.cleantext.gui;

import java.util.Arrays;

public enum CommandEnum {

    UNDO("Undo"),
    FIX_SPACES("Fix Spaces"),
    FIX_LINE_BREAKS("Fix Line Breaks"),
    JOIN_PARAGRAPHS("Join Paragraphs"),
    REMOVE_SPACES("Remove Spaces"),
    REMOVE_RETURNS("Remove Returns"),
    REMOVE_SPACES_AND_RETURNS("Remove Spaces and Returns"),
    REMOVE_QUOTE_PREFIX("Remove Quote Prefix"),
    REMOVE_EMPTY_LINES("Remove Empty Lines"),
    REMOVE_LINE_NUMBERS("Remove Line Numbers"),
    REMOVE_DUPLICATE_LINES("Remove Duplicate Lines"),
    TRIM_SPACES("Trim Spaces"),
    TRIM_EMPTY_LINES("Trim Empty Lines"),
    TRIM_SPACES_AND_EMPTY_LINES("Trim Spaces and Empty Lines"),
    REPLACE_TABS_WITH_SPACES("Tabs With Spaces"),
    REPLACE_SPACES_WITH_TABS("Spaces With Tabs"),
    REPLACE_TABS_WITH_FOUR_SPACES("Tabs With Four Spaces"),
    REPLACE_FOUR_SPACES_WITH_TAB("Four Spaces With Tab"),
    REPLACE_ELLIPSIS_TO_THREE_PERIODS("Ellipsis to Three Periods"),
    REPLACE_THREE_PERIODS_TO_ELLIPSIS("Three Periods to Ellipsis"),
    UNKNOWN("");

    private String command;

    CommandEnum(String command) {
        this.command = command;
    }

    public static CommandEnum fromValue(String value) {
        if (value == null) return UNKNOWN;
        return Arrays.stream(values()).filter(e -> e.command.equals(value)).findFirst().orElse(UNKNOWN);
    }

    @Override
    public String toString() {
        return command;
    }

}
