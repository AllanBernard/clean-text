package ee.bernard.cleantext.gui;

import ee.bernard.cleantext.processor.*;
import lombok.Setter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Setter
public class MainFrame extends JFrame implements ActionListener {

    private TextFieldComponent textField;
    private ButtonsComponent buttons;

    private int mainWindowHeight;
    private int mainWindowWidth;
    private int buttonComponentWidth;
    private boolean textComponentShouldWrap;
    private float textComponentFontSize;

    public void initialize() {
        int textComponentWidth = mainWindowWidth - buttonComponentWidth;
        textField = new TextFieldComponent(mainWindowHeight, textComponentWidth, textComponentFontSize, textComponentShouldWrap);
        buttons = new ButtonsComponent(mainWindowHeight, buttonComponentWidth, this);

        JPanel mainDisplay = new JPanel();
        mainDisplay.add(textField.getComponent());
        mainDisplay.add(buttons.getComponent());
        add(mainDisplay);

        setResizable(false);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String output = ProcessorUtil.convertLineEndingsToSystem(textField.getText());

        switch (CommandEnum.fromValue(e.getActionCommand())) {
            case UNDO:
                textField.undo();
                output = textField.getText();
                break;
            case FIX_SPACES:
                output = FixProcessor.fixSpaces(output);
                break;
            case FIX_LINE_BREAKS:
                output = FixProcessor.fixLineBreaks(output);
                break;
            case JOIN_PARAGRAPHS:
                output = FixProcessor.joinParagraphs(output);
                break;
            case REMOVE_SPACES:
                output = RemoveProcessor.removeSpaces(output);
                break;
            case REMOVE_RETURNS:
                output = RemoveProcessor.removeReturns(output);
                break;
            case REMOVE_SPACES_AND_RETURNS:
                output = RemoveProcessor.removeSpacesAndReturns(output);
                break;
            case REMOVE_QUOTE_PREFIX:
                output = RemoveProcessor.removeQuotePrefix(output);
                break;
            case REMOVE_EMPTY_LINES:
                output = RemoveProcessor.removeEmptyLines(output);
                break;
            case REMOVE_LINE_NUMBERS:
                output = RemoveProcessor.removeLineNumbers(output);
                break;
            case REMOVE_DUPLICATE_LINES:
                output = RemoveProcessor.removeDuplicateLines(output);
                break;
            case TRIM_SPACES:
                output = TrimProcessor.trimSpaces(output);
                break;
            case TRIM_EMPTY_LINES:
                output = TrimProcessor.trimEmptyLines(output);
                break;
            case TRIM_SPACES_AND_EMPTY_LINES:
                output = TrimProcessor.trimAndClear(output);
                break;
            case REPLACE_TABS_WITH_SPACES:
                output = ReplaceProcessor.replaceTabWithSpace(output);
                break;
            case REPLACE_SPACES_WITH_TABS:
                output = ReplaceProcessor.replaceSpaceWithTab(output);
                break;
            case REPLACE_TABS_WITH_FOUR_SPACES:
                output = ReplaceProcessor.replaceTabsWithFourSpaces(output);
                break;
            case REPLACE_FOUR_SPACES_WITH_TAB:
                output = ReplaceProcessor.replaceFourSpacesWithTabs(output);
                break;
            case REPLACE_ELLIPSIS_TO_THREE_PERIODS:
                output = ReplaceProcessor.replaceEllipsisWithThreePeriods(output);
                break;
            case REPLACE_THREE_PERIODS_TO_ELLIPSIS:
                output = ReplaceProcessor.replaceThreePeriodsWithEllipsis(output);
                break;
        }

        if (!output.equals(textField.getText())) textField.setText(output);
        buttons.UndoEnabled(textField.getUndoManager().canUndo());
    }

}
