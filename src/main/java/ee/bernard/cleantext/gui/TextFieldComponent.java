package ee.bernard.cleantext.gui;

import ee.bernard.cleantext.processor.UndoManager;
import lombok.AccessLevel;
import lombok.Getter;

import javax.swing.*;
import java.awt.*;

import static java.awt.Component.LEFT_ALIGNMENT;

@Getter
public class TextFieldComponent {

    private JPanel component;
    private UndoManager undoManager;
    @Getter(AccessLevel.PROTECTED)
    private JTextArea textArea;

    public TextFieldComponent(int componentHeight, int componentWidth, float fontSize, boolean shouldWrap) {
        component = new JPanel();
        undoManager = new UndoManager();

        textArea = new JTextArea();
        textArea.setWrapStyleWord(shouldWrap);
        textArea.setLineWrap(shouldWrap);
        textArea.setFont(textArea.getFont().deriveFont(fontSize));
        textArea.setComponentPopupMenu(new TextFieldPopup().getMenu());

        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(componentWidth, componentHeight));
        scrollPane.setAlignmentX(LEFT_ALIGNMENT);

        component.setLayout(new BoxLayout(component, BoxLayout.PAGE_AXIS));
        component.add(scrollPane);
    }

    public String getText() {
        return textArea.getText();
    }

    public void setText(String text) {
        undoManager.addState(textArea.getText());
        textArea.setText(text);
    }

    public void undo() {
        if (undoManager.canUndo()) textArea.setText(undoManager.undoEdit());
    }

}
