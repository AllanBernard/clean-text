package ee.bernard.cleantext.processor;

public class ReplaceProcessor {

    private ReplaceProcessor() {
        throw new IllegalStateException("Class should not be instantiated directly");
    }

    public static String replaceTabWithSpace(String input) {
        return input.replace("\t", " ");
    }

    public static String replaceSpaceWithTab(String input) {
        return input.replace(" ", "\t");
    }

    public static String replaceTabsWithFourSpaces(String input) {
        return input.replace("\t", "    ");
    }

    public static String replaceFourSpacesWithTabs(String input) {
        return input.replace("    ", "\t");
    }

    public static String replaceEllipsisWithThreePeriods(String input) {
        return input.replace("\u2026", "...");
    }

    public static String replaceThreePeriodsWithEllipsis(String input) {
        return input.replace("...", "\u2026");
    }

}
