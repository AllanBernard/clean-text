package ee.bernard.cleantext.processor;

import java.util.ArrayList;
import java.util.List;

public class RemoveProcessor {

    private RemoveProcessor() {
        throw new IllegalStateException("Class should not be instantiated directly");
    }

    public static String removeSpaces(String input) {
        return input.replace(" ", "");
    }

    public static String removeReturns(String input) {
        return input.replace(System.lineSeparator(), "");
    }

    public static String removeSpacesAndReturns(String input) {
        return removeSpaces(removeReturns(input));
    }

    public static String removeEmptyLines(String input) {
        String output = ProcessorUtil.removeConsecutiveCharacters(input, System.lineSeparator(), 2);
        return TrimProcessor.trimEmptyLines(output);
    }

    public static String removeQuotePrefix(String input) {
        String[] lines = ProcessorUtil.createArrayOfLines(input);
        for (int i = 0; i < lines.length; i++) {
            lines[i] = lines[i].replaceAll("^\\>+", "");
            lines[i] = lines[i].replaceAll("^\\s+", "");
        }
        return ProcessorUtil.createStringOfLines(lines);
    }

    public static String removeLineNumbers(String input) {
        String[] lines = ProcessorUtil.createArrayOfLines(input);
        for (int i = 0; i < lines.length; i++) {
            lines[i] = lines[i].replaceAll("^\\d+:+\\s+", "");
            lines[i] = lines[i].replaceAll("^\\d+\\s+", "");
        }
        return ProcessorUtil.createStringOfLines(lines);
    }

    public static String removeDuplicateLines(String input) {
        String[] lines = ProcessorUtil.createArrayOfLines(input);
        int numberOfLines = lines.length;
        if (numberOfLines == 1) return lines[0];
        List<String> output = new ArrayList<>();
        for (int i = 0; i < numberOfLines; i++) {
            while ((i < (numberOfLines - 1)) && (lines[i].equals(lines[i + 1]))) i++;
            output.add(lines[i]);
        }
        return ProcessorUtil.createStringOfLines(output.toArray(new String[0]));
    }
}
