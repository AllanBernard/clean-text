package ee.bernard.cleantext.processor;

public class TrimProcessor {

    private TrimProcessor() {
        throw new IllegalStateException("Class should not be instantiated directly");
    }

    public static String trimSpaces(String input) {
        return input.replaceAll("^[ ]+|[ ]+$", "");
    }

    public static String trimTabs(String input) {
        return input.replaceAll("^[\t]+|[\t]+$", "");
    }

    public static String trimEmptyLines(String input) {
        input = ProcessorUtil.convertLineEndingsToWin(input);
        return input.replaceAll("^[\r\n]+|[\r\n]+$", "");
    }

    public static String trimAndClear(String input) {
        return input.trim();
    }

}
