package ee.bernard.cleantext.processor;

public class FixProcessor {

    private FixProcessor() {
        throw new IllegalStateException("Class should not be instantiated directly");
    }

    public static String fixSpaces(String input) {
        String[] lines = ProcessorUtil.createArrayOfLines(input);
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (line.isEmpty()) continue;
            line = TrimProcessor.trimSpaces(line);
            line = ProcessorUtil.removeConsecutiveCharacters(line, " ", 2);
            lines[i] = line;
        }
        return ProcessorUtil.createStringOfLines(lines);
    }

    public static String fixLineBreaks(String input) {
        input = ProcessorUtil.clearEmptyLines(input);
        input = concatLinesToParagraphs(input);
        input = TrimProcessor.trimEmptyLines(input);
        return ProcessorUtil.removeConsecutiveCharacters(input, System.lineSeparator(), 3);
    }

    public static String concatLinesToParagraphs(String input) {
        String[] lines = ProcessorUtil.createArrayOfLines(input);
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (line.isEmpty()) {
                output.append(System.lineSeparator());
                continue;
            }
            output.append(line);
            if (ProcessorUtil.isEndOfParagraph(lines, i)) output.append(System.lineSeparator());
        }
        return output.toString();
    }

    public static String joinParagraphs(String input) {
        return ProcessorUtil.removeConsecutiveCharacters(input, System.lineSeparator(), 2);
    }

}
