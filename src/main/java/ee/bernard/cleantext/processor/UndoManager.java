package ee.bernard.cleantext.processor;

import java.util.Stack;

public class UndoManager {

    private Stack<String> stack;

    public UndoManager() {
        stack = new Stack<>();
    }

    public void addState(String text) {
        stack.push(text);
    }

    public String undoEdit() {
        if (stack.empty()) return null;
        return stack.pop();
    }

    public boolean canUndo() {
        return !stack.empty();
    }

}
