package ee.bernard.cleantext.processor;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ProcessorUtil {

    private ProcessorUtil() {
        throw new IllegalStateException("Class should not be instantiated directly");
    }

    public static String convertLineEndingsToSystem(String input) {
        return input.replaceAll("\r\n|\r|\n", System.lineSeparator());
    }

    public static String convertLineEndingsToWin(String input) {
        return input.replaceAll("\r\n|\r|\n", "\r\n");
    }

    public static String[] createArrayOfLines(String input) {
        return input.split(System.lineSeparator(), -1);
    }

    public static String createStringOfLines(String[] input) {
        if (input.length == 1 && input[0].isEmpty()) return "";
        return Arrays.stream(input).collect(Collectors.joining(System.lineSeparator()));
    }

    public static String removeConsecutiveCharacters(String input, String characterToCheck, int count) {
        StringBuilder toReplace = new StringBuilder();
        StringBuilder replaceWith = new StringBuilder();
        for (int i = 0; i < count; i++) {
            toReplace.append(characterToCheck);
            if (i < count - 1) replaceWith.append(characterToCheck);
        }
        if (input.contains(toReplace)) {
            return removeConsecutiveCharacters(input.replace(toReplace, replaceWith), characterToCheck, count);
        } else {
            return input;
        }
    }

    public static String clearEmptyLines(String input) {
        String[] lines = createArrayOfLines(input);
        for (int i = 0; i < lines.length; i++) {
            if (hasNoVisibleCharacters(lines[i])) lines[i] = "";
        }
        input = createStringOfLines(lines);
        return input;
    }

    public static boolean isSecondEmptyLineInArray(String[] array, int currentPosition) {
        if (currentPosition < 1) return false;
        if (currentPosition < 2) return array[0].isEmpty() && array[1].isEmpty();
        return array[currentPosition].isEmpty()
                && array[currentPosition - 1].isEmpty()
                && array[currentPosition - 2].isEmpty();
    }

    public static boolean isLastFilledLineInArray(String[] array, int currentPosition) {
        currentPosition++;
        for (; currentPosition < array.length; currentPosition++) {
            if (!array[currentPosition].isEmpty()) return false;
        }
        return true;
    }

    public static boolean isEndOfParagraph(String[] array, int currentPosition) {
        return !isLastFilledLineInArray(array, currentPosition)
                && currentPosition < array.length
                && array[currentPosition + 1].isEmpty();
    }

    public static boolean hasNoVisibleCharacters(String input) {
        return TrimProcessor.trimAndClear(input).isEmpty();
    }
}
