package ee.bernard.cleantext;

import ee.bernard.cleantext.configuration.SpringContext;
import ee.bernard.cleantext.gui.MainFrame;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public final class CleanTextApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringContext.class);
        MainFrame mainFrame = (MainFrame) context.getBean("mainFrame");
        mainFrame.initialize();
    }

}
